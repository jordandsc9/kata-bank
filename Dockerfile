FROM openjdk:8-jre-alpine

Volume /tmp

ADD /target/*.jar katabank_jordan-1.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/katabank_jordan-1.0.1-SNAPSHOT.jar"]
