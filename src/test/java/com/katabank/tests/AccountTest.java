package com.katabank.tests;

import com.katabank.bo.Account;
import com.katabank.bo.OperationCode;
import com.katabank.bo.TransactionRecorder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AccountTest {

    @Mock
    TransactionRecorder recorder;

    private Account account;

    @BeforeEach
    public void initialise() {
        account = new Account(recorder);
    }

    @Test
    public void testCheckBalanceTest(){
        account.setBalance(200.0);
        assertEquals(200.0, account.getBalance());
    }

    @Test
    public void testRecordOperation(){
        account.setBalance(50.0);
        account.recordOperation(5.0, OperationCode.DEPOSITE, LocalDate.of(2021, 11, 12));
        verify(recorder).addToRecord("5.0","50.0", OperationCode.DEPOSITE.toString(), LocalDate.of(2021, 11, 12));
    }
}
