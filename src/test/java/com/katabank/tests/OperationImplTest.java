package com.katabank.tests;

import com.katabank.implementation.OperationImpl;
import com.katabank.bo.OperationCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static java.time.LocalDate.now;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class OperationImplTest {

    @Spy
    private OperationImpl operationImpl;

    @Test
    public void testWithdrawalOperationWithDate(){
        operationImpl.transaction(20.0, OperationCode.WITHDRAWAL, LocalDate.of(2021, 9, 1));
        verify(operationImpl).withdrawal(20.0,LocalDate.of(2021, 9, 1));
    }

    @Test
    public void testWithdrawalOperationWithoutDate(){
        operationImpl.transaction(20.0, OperationCode.WITHDRAWAL, null);
        verify(operationImpl).withdrawal(20.0, null);
    }

    @Test
    public void testDepositOperation(){
        operationImpl.transaction(50.0, OperationCode.DEPOSITE, now());
        verify(operationImpl).deposit(50.0,now());
    }

    @Test
    public void testDepositOperationWithoutDate(){
        operationImpl.transaction(50.0, OperationCode.DEPOSITE, null);
        verify(operationImpl).deposit(50.0, null);
    }

    @Test
    public void testPrintAllTransactionOperation(){
        operationImpl.transaction(100.0, OperationCode.DEPOSITE, LocalDate.of(2021, 9, 3));
        operationImpl.transaction(200.0, OperationCode.WITHDRAWAL ,LocalDate.of(2021, 9, 3));
        operationImpl.transaction(0.0, OperationCode.VIEW_ACCOUNT_RECORD, null);
        verify(operationImpl).printAllTransaction();
    }

    @Test
    public void testPrintBalanceOperation(){
        operationImpl.transaction(0.0, OperationCode.VIEW_SOLDE, null);
        verify(operationImpl).printBalance();
    }

}
