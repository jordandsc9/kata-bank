package com.katabank.tests;

import com.katabank.bo.OperationCode;
import com.katabank.bo.Statement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StatementTest {

    private Statement statement;

    @BeforeEach
    public void initilise(){
        statement = new Statement("20", OperationCode.DEPOSITE.toString(), "50", LocalDate.of(2021, 11, 21));
    }

    @Test
    public void testStatement(){
        assertEquals("20", statement.getAmount());
        assertEquals("50", statement.getBalance());
        assertEquals("DEPOSITE", statement.getOperation());
        assertEquals(LocalDate.of(2021, 11, 21), statement.getDate());
    }
}
