package com.katabank.tests;

import com.katabank.bo.OperationCode;
import com.katabank.bo.TransactionRecorder;
import com.katabank.tools.LineParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.time.LocalDate.now;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class LineParserTest {

    @Spy
    private LineParser lineParser;

    TransactionRecorder accountRecord;

    @BeforeEach
    public void initialise(){
        accountRecord = new TransactionRecorder();
    }

    @Test
    public void testPrintHistoyToConsole(){
        accountRecord.addToRecord("200", "5000", OperationCode.WITHDRAWAL.toString(), now());
        lineParser.parseHistory(accountRecord);
        assertEquals("Date || Amount || Operation || Balance\n" +
                now()+ " || 200 || WITHDRAWAL || 5000\n", lineParser.getSb().toString());
    }

}
