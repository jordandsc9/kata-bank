package com.katabank.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class TransactionRecorder {

    private List<Statement> record = new ArrayList<>();

    public void addToRecord(String amount, String balance, String operation, LocalDate date){
        record.add(new Statement(amount, operation, balance, date));
    }
}
