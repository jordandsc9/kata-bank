package com.katabank.bo;

public enum OperationCode {
    WITHDRAWAL("0"), DEPOSITE("1"), VIEW_ACCOUNT_RECORD("2"), VIEW_SOLDE("3");

    public final String label;

    OperationCode(String label) {
        this.label = label;
    }
}
