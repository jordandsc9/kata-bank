package com.katabank.bo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class Statement {

    private String amount;
    private String operation;
    private String balance;
    private LocalDate date;

}
