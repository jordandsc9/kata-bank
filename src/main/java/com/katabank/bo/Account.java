package com.katabank.bo;

import lombok.*;

import java.time.LocalDate;

@Data
public class Account {

    private double balance = 0;

    @NonNull
    @Setter(AccessLevel.NONE)
    private final TransactionRecorder recorder;

    public Account(TransactionRecorder currentRecorder){
        this.recorder = currentRecorder;
    }

    public void recordOperation(double amount, OperationCode operationCode, LocalDate date){
        recorder.addToRecord(Double.toString(amount), Double.toString(getBalance()), operationCode.toString(), date);
    }
}