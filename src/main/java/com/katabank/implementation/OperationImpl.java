package com.katabank.implementation;

import com.katabank.bo.Account;
import com.katabank.definition.Operation;
import com.katabank.bo.OperationCode;
import com.katabank.bo.TransactionRecorder;
import com.katabank.tools.LineParser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.util.Objects;

import static com.katabank.bo.OperationCode.*;

@NoArgsConstructor
public class OperationImpl implements Operation {

    @Getter
    private final Account account = new Account(new TransactionRecorder());

    private final LineParser lineparser = new LineParser();

    public void withdrawal(double amount, LocalDate date){
        this.account.recordOperation(amount, WITHDRAWAL, Objects.requireNonNullElseGet(date, LocalDate::now));
        this.account.setBalance(this.account.getBalance() - amount);
    }

    public void deposit(double amount, LocalDate date){
        this.account.recordOperation(amount, DEPOSITE, Objects.requireNonNullElseGet(date, LocalDate::now));
        this.account.setBalance(this.account.getBalance() + amount);
    }

    public void printAllTransaction(){
        this.lineparser.parseHistory(this.account.getRecorder());
    }

    public void printBalance(){
        this.lineparser.parseBalance(Double.toString(this.account.getBalance()));
    }

    public void transaction(double amount, OperationCode codeTransaction, LocalDate date){
        switch (codeTransaction){
            case WITHDRAWAL:
                withdrawal(amount, date);
                break;
            case DEPOSITE:
                deposit(amount, date);
                break;
            case VIEW_ACCOUNT_RECORD:
                printAllTransaction();
                break;
            case VIEW_SOLDE:
                printBalance();
                break;
        }
    }
}
