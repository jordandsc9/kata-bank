package com.katabank.definition;

import com.katabank.bo.OperationCode;

import java.time.LocalDate;

public interface Operation {

    void withdrawal(double amount, LocalDate date);
    void deposit(double amount, LocalDate date);
    void printAllTransaction();
    void printBalance();
    void transaction(double amount, OperationCode codeTransaction, LocalDate date);
}
