package com.katabank.application;

import com.katabank.demo.ApplicationDemo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KatabankJordanApplication {

	//Demo is started on application launch
	public static void main(String[] args) {
		ApplicationDemo demo = new ApplicationDemo();
		SpringApplication.run(KatabankJordanApplication.class, args);
		demo.startDemo1();
		demo.startDemo2();

	}

}
