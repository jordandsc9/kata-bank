package com.katabank.demo;

import com.katabank.definition.Operation;
import com.katabank.implementation.OperationImpl;
import com.katabank.bo.OperationCode;

import java.time.LocalDate;

public class ApplicationDemo {

    private final Operation operation = new OperationImpl();
    private static final LocalDate DEMO_DAY = LocalDate.of(2021, 9, 1);

    /*
    Given a client makes a deposit of 500 on 01 septembre 2021
    And a deposit of 2500 on 02 septembre 2021
    And a withdrawal of 1000 on 02 septembre 2021
     */
    public ApplicationDemo(){
        LocalDate.of(2021, 9, 1);
        operation.transaction(500.0, OperationCode.DEPOSITE, DEMO_DAY);
        operation.transaction(2500.0, OperationCode.DEPOSITE, DEMO_DAY);
        operation.transaction(1000.0, OperationCode.WITHDRAWAL, DEMO_DAY);
    }

    /*
    When he prints his account record
    Then he would see
    Date || Amount || Operation || Balance
    01 septembre 2021 || 500.0 || DEPOSITE || 0.0
    02 septembre 2021 || 2500.0 || DEPOSITE || 500.0
    02 septembre 2021 || 1000.0 || WITHDRAWAL || 3000.0
     */
    public void startDemo1 (){
        operation.transaction(0, OperationCode.VIEW_ACCOUNT_RECORD, null);
    }

    /*
    When he prints his account balance
    Then he would see
    Balance : 2000.0
     */
    public void startDemo2 (){
        operation.transaction(0, OperationCode.VIEW_SOLDE, null);
    }

    }
