package com.katabank.tools;

import com.katabank.bo.Statement;
import com.katabank.bo.TransactionRecorder;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class LineParser {

    private static final String RECORD_HEADER = "Date || Amount || Operation || Balance\n";
    private static final String SOLDE_HEADER = "Balance : ";
    private static final String LINE_SEPARATOR = " || ";

    @Setter(AccessLevel.NONE)
    private StringBuilder sb;

    public void printToConsole(StringBuilder sentence){
        System.out.print(sentence);
    }

    public void printLnToConsole(StringBuilder sentence){
        System.out.println(sentence);
    }

    public void parseHistory (TransactionRecorder accountRecord){
        sb = new StringBuilder();
        sb.append(RECORD_HEADER);
        for (Statement statementLine: accountRecord.getRecord()) {
            sb.append(statementLine.getDate());
            sb.append(LINE_SEPARATOR);
            sb.append(statementLine.getAmount());
            sb.append(LINE_SEPARATOR);
            sb.append(statementLine.getOperation());
            sb.append(LINE_SEPARATOR);
            sb.append(statementLine.getBalance());
            sb.append('\n');
        }
        printToConsole(sb);
    }

    public void parseBalance(String balance){
        sb = new StringBuilder();
        sb.append(SOLDE_HEADER);
        sb.append(balance);
        printLnToConsole(sb);
    }
}
