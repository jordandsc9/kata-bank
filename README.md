### Kata Bank Project

This project is made with Spring Boot and Java 11.

### Local Testing

To start this application you either need Docker or a JDK 11 and maven configured:

For Docker Deploy, type at the root of the repository:

> Docker-compose up

For normal deploy:
> Execute the main class:
> https://gitlab.com/jordandsc9/kata-bank/-/blob/master/src/main/java/com/katabank/application/KatabankJordanApplication.java

Or
> Execute this command line at the root of the repo:
> mvn spring-boot:run

The demo is available here:
> https://gitlab.com/jordandsc9/kata-bank/-/blob/master/src/main/java/com/katabank/demo/ApplicationDemo.java

### User story
Requirements:
- Deposit and Withdrawal
- Account statement (date, amount, balance)
- Statement printing

User Stories
US 1:
In order to save money
As a bank client
I want to make a deposit in my account

US 2:
In order to retrieve some or all of my savings
As a bank client
I want to make a withdrawal from my account

US 3:
In order to check my operations
As a bank client
I want to see the history (operation, date, amount, balance) of my operations."


### CI/CD with Auto DevOps

Auto DevOps is enabled for this project, you can access the pipeline to check unit test.

### Improvement Idea

- Turn the application into a Rest api
- Make a swagger
- Set up a database (postgres, mariadb, oracle...)
- Add a method to open and close a client account
- Add method to send money to other account (in the application scope)
